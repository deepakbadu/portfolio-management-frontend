import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StockComponent } from "./stock/stock.component";
import { TransactionComponent } from "./transaction/transaction.component";

const routes: Routes = [
  {path: "stock", component: StockComponent},
  {path: "transaction", component: TransactionComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponent = [StockComponent , TransactionComponent]
