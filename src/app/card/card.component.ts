import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {
 
  @Input() cardTitle: string = ''
  @Input() totalUnit: number = 0; 
  @Input() soldAmount: number = 0;
  @Input() overAllProfit: number = 0;
  @Input() totalInvestment: number = 0;
  @Input() currentAmount:number = 0;
  constructor() { }

  ngOnInit(): void {
  }

}
