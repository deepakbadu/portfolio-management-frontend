import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-stock',
  templateUrl: './stock.component.html',
  styleUrls: ['./stock.component.css']
})
export class StockComponent implements OnInit {

  stockName:string = ''
  constructor(private userService:UserService, private router:Router) { }

  ngOnInit(): void {
  }
  submitStock(){
    const data = { stockName: this.stockName}
    this.userService.submitStock(data).subscribe(value =>{
      this.router.navigate(["/"])
    })
  }

}
