import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http:HttpClient) { }
  getData() {
    let url = "http://localhost:8000/dashboard";
    return this.http.get(url)
  }
  getList(){
    let listUrl = "http://localhost:8000"
    return this.http.get(listUrl)
  }
  getStock() {
    let stockUrl = "http://localhost:8000/stock"
    return this.http.get(stockUrl)
  }
  submitData(data:any) {
    let url = "http://localhost:8000/add"
    return this.http.post(url,data)
  }
  submitStock(data:any){
    let url = "http://localhost:8000/stock"
    return this.http.post(url,data)
  }
  getIndividual(){
    let url = "http://localhost:8000/individualStock"
    return this.http.get(url)
  }
}

