import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from "../user.service";

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.css']
})

export class TransactionComponent implements OnInit,AfterViewInit {
  
  stock: any = []
  stockName:string = 'Choose any stock'
  transactionType:string = ''
  quantity = null
  price = null
  constructor(private userService:UserService ,private router:Router) { 
  }
  ngAfterViewInit(): void {
    this.userService.getStock().subscribe(data => {
      this.stock = data
      this.stockName = this.stock && this.stock[0]?.stockName
    })
  }
  ngOnInit(): void {
  }
  selectedItem(stockName:any) {
    this.stockName = stockName
  }
  submitData() {
    const data = { stockName: this.stockName, transactionType: this.transactionType, quantity:this.quantity,price:this.price }
    this.userService.submitData(data).subscribe(value => {
     this.router.navigate(["/"])
   }) 
  }
}
