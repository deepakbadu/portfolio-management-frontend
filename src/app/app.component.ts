import { Component } from '@angular/core'
import { UserService } from "./user.service";
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  getData(dash: []) {
    return JSON.stringify(dash)
  }
  invividualData:any = []
  dashboard: any = []
  appname = 'Portfolio Management';
  list: any;
  constructor(private userService:UserService){
    this.userService.getData().subscribe(data => {
      this.dashboard = data
    })
    this.userService.getList().subscribe(listData => {
      this.list = listData
    })
    this.userService.getIndividual().subscribe(data =>{
      this.invividualData = data
      console.log(this.invividualData)
    })
  }
}